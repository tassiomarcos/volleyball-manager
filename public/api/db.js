// const knex = require("knex");

// module.exports = knex({
//   client: "sqlite",
//   connection: { filename: "./database.sqlite" },
// });

const sqlite = require("sqlite3").verbose();
const path = require("path");

const storage = path.resolve(__dirname, "./database.sqlite");

const db = new sqlite.Database(storage, sqlite.OPEN_READWRITE, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Connected to database");
  }
});

module.exports = db;
