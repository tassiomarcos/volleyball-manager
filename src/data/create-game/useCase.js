import {
  requestManager,
  successManager,
  failureManager,
  requestAddTeam,
  successAddTeam,
  failureAddTeam,
  requestCreateGame,
  successCreateGame,
  failureCreateGame,
  actions,
} from "./";

import db from "../../configs/database";
import { createRegularChampionship } from "../../scripts/create-championship-table";

export function createManager(dispatch) {
  return async function (name, age, country) {
    try {
      dispatch(requestManager());

      db.serialize(() => {
        const stmt = db.prepare(
          "INSERT INTO managers(name, age, country, prestige, intelligence) VALUES (?, ?, ?, 10, 10)",
          [name, age, country]
        );
        stmt.run();
        stmt.finalize();

        dispatch({ type: actions.SUCCESS_MANAGER });
      });
    } catch (e) {
      return dispatch(failureManager(e));
    }
  };
}

export function addTeam(dispatch) {
  return function (clubId) {
    db.serialize(function () {
      db.all("SELECT * FROM managers ORDER BY id DESC LIMIT 1", function (
        err,
        callback
      ) {
        if (err) {
          console.log(err);
        } else {
          const id = callback[0].id;
          db.run(
            `UPDATE clubs SET managerid = ${id} where clubs.id = ${clubId}`
          );
          db.run(
            `UPDATE managers SET clubid = NULL where managers.clubid = ${clubId}`
          );
          db.run(
            `UPDATE managers SET clubid = ${clubId} where managers.id = ${id}`
          );

          dispatch(successAddTeam());
        }
      });
    });
  };
}

export function getClubByChampionshipId(dispatch) {
  return function (id) {
    console.log(id);
    db.serialize(function () {
      db.all(
        `SELECT name, id from clube_championship cross join clubs on clube_championship.clubid = clubs.id where clube_championship.championshipid = ${id} order by clubs.id`,
        function (err, callback) {
          if (err) {
            console.log(err);
          } else {
            console.log(callback);
            dispatch({
              type: actions.GET_CLUBS_BY_CHAMPIONSHIP_ID,
              payload: callback.filter((e) => e.id),
            });
          }
        }
      );
    });
  };
}

export function getLegueChampionships(dispatch) {
  return async function () {
    try {
      db.serialize(function () {
        db.all("select * from championship where type='league'", function (
          err,
          callback
        ) {
          if (err) {
            console.log(err);
          } else {
            dispatch({ type: actions.GET_CHAMPIONSHIP, payload: callback });
          }
        });
      });
    } catch (e) {
      dispatch({});
    }
  };
}

export function createGames(dispatch) {
  db.serialize(function () {
    db.each("SELECT id FROM championship", function (err, { id }) {
      if (err) {
        console.log(err);
      } else {
        db.all(
          `SELECT name, id from clube_championship cross join clubs on clube_championship.clubid = clubs.id where clube_championship.championshipid = ${id} order by clubs.id`,
          function (err, callback) {
            if (err) {
              console.log(err);
            } else {
              const jogos = createRegularChampionship(callback);

              if (jogos.length) {
                const stmt = db.prepare();
              }
            }
          }
        );
      }
    });
  });
}
