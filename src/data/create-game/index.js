export const actions = {
  CREATE_MANAGER: "CREATE_MANAGER",
  SUCCESS_MANAGER: "SUCCESS_MANAGER",
  FAILURE_MANAGER: "FAILURE_MANAGER",
  REQUEST_ADD_TEAM: "REQUEST_ADD_TEAM",
  SUCCESS_ADD_TEAM: "SUCCES_ADD_TEAM",
  FAILURE_ADD_TEAM: "FAILURE_ADD_TEAM",
  REQUEST_CREATE_GAME: "REQUEST_CREATE_GAME",
  SUCCESS_CREATE_GAME: "SUCCES_CREATE_GAME",
  FAILURE_CREATE_GAME: "FAILURE_CREATE_GAME",
  RESET: "RESET",
  GET_CHAMPIONSHIP: "GET_CHAMPIONSHIP",
  GET_CLUBS_BY_CHAMPIONSHIP_ID: "GET_CLUBS_BY_CHAMPIONSHIP_ID",
};

export const requestManager = () => ({ type: actions.CREATE_MANAGER });
export const successManager = (callback) => ({
  type: actions.SUCCESS_MANAGER,
  payload: callback,
});
export const failureManager = (error) => ({
  type: actions.FAILURE_MANAGER,
  payload: error,
});
export const requestAddTeam = () => ({ type: actions.REQUEST_ADD_TEAM });
export const successAddTeam = () => ({ type: actions.SUCCESS_ADD_TEAM });
export const failureAddTeam = (error) => ({
  type: actions.FAILURE_ADD_TEAM,
  payload: error,
});
export const requestCreateGame = () => ({ type: actions.REQUEST_CREATE_GAME });
export const successCreateGame = () => ({ type: actions.SUCCESS_CREATE_GAME });
export const failureCreateGame = (error) => ({
  type: actions.FAILURE_CREATE_GAME,
  payload: error,
});
