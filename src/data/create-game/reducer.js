import { actions } from "./";

const initialState = {
  success: false,
  isLoading: true,
  isLoadingCreateGame: true,
};

export default (state = initialState, action) => {
  if (actions.SUCCESS_MANAGER === action.type) {
    console.log(action);
  }
  switch (action.type) {
    case actions.REQUEST_CREATE_GAME:
      return {
        ...state,
        isLoadingCreateGame: true,
        success: false,
        error: null,
      };
    case actions.CREATE_MANAGER:
    case actions.REQUEST_ADD_TEAM:
      return { ...state, isLoading: true, success: false };
    case actions.SUCCESS_MANAGER:
    case actions.SUCCESS_ADD_TEAM:
    case actions.SUCCESS_CREATE_GAME:
      return {
        ...state,
        isLoading: false,
        success: true,
        isLoadingCreateGame: false,
        tag: action?.payload,
      };
    case actions.FAILURE_MANAGER:
    case actions.FAILURE_ADD_TEAM:
    case actions.FAILURE_CREATE_GAME:
      return {
        ...state,
        isLoading: false,
        success: false,
        error: action.payload,
        isLoadingCreateGame: false,
      };
    case actions.GET_CHAMPIONSHIP:
      return { ...state, championships: action.payload };
    case actions.GET_CLUBS_BY_CHAMPIONSHIP_ID:
      return { ...state, clubs: action.payload };
    case actions.RESET:
      return initialState;
    default:
      return state;
  }
};
