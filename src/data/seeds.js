import db from "../configs/database";

export function initDatabase() {
  const existSchames = window.localStorage.getItem("exist-schmeas");

  if (!existSchames) {
    db.serialize(function () {
      db.run(
        `CREATE TABLE CLUBS (
            id INTEGER PRIMARY KEY AUTOINCREMENT, 
            name TEXT, 
            country TEXT, 
            prestige TEXT, 
            financeId INTEGER,
            FOREIGN KEY(financeId) REFERENCES FINANCE(id))`
      );
      db
        .run
        // "CREATE TABLE MANAGERS (id INTEGER PRIMARY KEY  AUTOINCREMENT, name TEXT) "
        ();
    });

    localStorage.setItem("exist-schmeas", "true");
  } else {
    return;
  }
}

export function dropTables() {
  db.serialize(function () {
    db.run("DROP TABLE MANAGERS");
    db.run("DROP TABLE FINANCE");
    db.run("DROP TABLE CLUBS");
  });

  localStorage.clear();
}
