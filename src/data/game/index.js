export const actions = {
  SUCCESS_RUN_SCRIPT: "SUCCESS_RUN_SCRIPT",
  REQUEST_RUN_SCRIPT: "REQUEST_RUN_SCRIPT",
  FAILURE_RUN_SCRIPT: "FAILURE_RUN_SCRIPT",
};

export const request = () => ({ type: actions.REQUEST_RUN_SCRIPT });
export const success = (payload) => ({
  type: actions.SUCCESS_RUN_SCRIPT,
  payload,
});
export const failure = (e) => ({ type: actions.FAILURE_RUN_SCRIPT, e });
