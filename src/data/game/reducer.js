import { actions } from ".";

const initialState = {
  championships: null,
  clubs: null,
  players: null,
  games: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.REQUEST_RUN_SCRIPT:
      return {
        ...state,
        runningScript: true,
        success: false,
        championships: null,
        clubs: null,
        players: null,
        games: null,
        myClub: null,
        me: null,
      };
    case actions.SUCCESS_RUN_SCRIPT:
      return {
        ...state,
        runningScript: false,
        clubs: action.payload.clubs,
        me: action.payload.me,
        myClub: action.payload.myClub,
      };
    case actions.FAILURE_RUN_SCRIPT:
      return { ...state };
    default:
      return state;
  }
};
