import { request, success, failure } from ".";
import { asyncStorage } from "../../configs/storage";
import { schemas } from "../schemas";

export function loadGame(dispatch) {
  return async function () {
    try {
      dispatch(request());
      const clubs = await asyncStorage.get(schemas.clubs);
      const managers = await asyncStorage.get(schemas.managers);
      const championships = await asyncStorage.get(schemas.championships);
      const me = managers[0];

      const myClub = clubs.filter((item) => item.id === me.team);

      dispatch(
        success({ clubs, managers, championships, myClub: myClub[0], me })
      );
    } catch (e) {
      dispatch(failure(e));
    }
  };
}
