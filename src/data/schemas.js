export const schemas = {
  managers: "managers",
  clubs: "clubs",
  championships: "championships",
  players: "players",
};
