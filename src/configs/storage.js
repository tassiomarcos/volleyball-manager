import { jwt } from "./encrypt";

async function get(key, callback) {
  try {
    let data = localStorage.getItem(key);
    const decrypt = JSON.parse(await jwt.decrypt(data));

    return decrypt;
  } catch (e) {
    if (callback) {
      callback(e);
    }
  }
}

async function set(key, data, callback) {
  try {
    const encrypt = await jwt.encrypt(data);
    localStorage.setItem(key, encrypt);

    if (callback) {
      callback(null, "sucess");
    }
  } catch (e) {
    if (callback) {
      callback(e);
    }
    throw new Error(e);
  }
}

export const asyncStorage = { get, set };
