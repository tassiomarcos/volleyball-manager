import { createStore, applyMiddleware } from "redux";
import reducers from "../data/reducers";
import logger from "redux-logger";
import thunk from "redux-thunk";

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
