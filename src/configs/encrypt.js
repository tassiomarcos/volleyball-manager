import Crypto from "crypto-js";
import { cryptoKey } from "./key";

async function encrypt(data) {
  try {
    const string = JSON.stringify(data);
    return Crypto.AES.encrypt(string, cryptoKey).toString();
  } catch (e) {
    throw new Error(e);
  }
}

async function decrypt(jwt) {
  try {
    const byte = Crypto.AES.decrypt(jwt, cryptoKey);
    return byte.toString(Crypto.enc.Utf8);
  } catch (e) {
    throw new Error(e);
  }
}

export const jwt = {
  encrypt,
  decrypt,
};
