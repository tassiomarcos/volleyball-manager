import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { InitialPage } from "../pages/InitialPage";
import { CreateNewGame } from "../pages/CreateNewGame";
import { AddTeamLeague } from "../pages/AddTeamLeague";
import { CreateGame } from "../pages/CreateGame";
import { Home } from "../pages/Home";

export function Routes() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          {/* <InitialPage /> */}
          <CreateGame />
        </Route>
        <Route path="/create-new-game">
          <CreateNewGame />
        </Route>
        <Route path="/add-team-league">
          <AddTeamLeague />
        </Route>
        <Route path="/create-game">
          <CreateGame />
        </Route>
        <Route>
          <Home path="/home" />
        </Route>
      </Switch>
    </Router>
  );
}
