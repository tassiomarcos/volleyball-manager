import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Routes } from './configs/routes';
import { Provider } from 'react-redux';
import store from './configs/store';

function App() {
  return ( 
    <Provider store={store}>
      <Routes />
    </Provider>
  )
}

export default App;
