import React from "react";
import styled, { keyframes } from "styled-components";
import { Container } from "./container";

const rotate = keyframes`
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
`;

const Component = styled.div`
  box-sizing: border-box;
  height: 60px;
  width: 60px;
  margin: 80px;
  border: 0px solid blue;
  border-radius: 50%;
  box-shadow: 0 -20px 0 24px blue inset;
  animation: ${rotate} 1s linear infinite;
  margin: 0;
  padding: 0;
`;

const Screen = styled.div`
  height: 780px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Text = styled.p`
  font-size: 16px;
  font-weight: bold;
  color: blue;
  text-align: center;
  margin: 0;
  padding: 0;
  margin-top: 8px;
`;

export function Spinner() {
  return (
    <Screen>
      <Component></Component>
      <Text>Carregando</Text>
    </Screen>
  );
}
