import React, { useEffect } from "react";
import styled from "styled-components";

const Select = styled.select`
  border: none;
  height: 45px;
  width: 260px;
  background: white;
  border: 1px solid blue;
  border-radius: 4px;
  color: blue;
  font-size: 15px;
`;
const Option = styled.option``;

export function Selection(props) {
  return (
    <Select defaultValue={props.defaultValue || null} onChange={props.onChange}>
      {!props.custom
        ? props.data.map((e) => {
            return (
              <Option key={e.nome} value={e.nome}>
                {e.nome}
              </Option>
            );
          })
        : props.children}
    </Select>
  );
}
