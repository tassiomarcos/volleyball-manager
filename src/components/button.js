import React from "react";
import { Link } from "react-router-dom";
import styled, { css } from "styled-components";

const styles = css`
  height: 45px;
  width: 260px;
  border: none;
  border-radius: 4px;
  background: white;
  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  background: blue;
  font-size: 18px;
`;

const Component = styled(Link)`
  ${styles}
`;

const Clickable = styled.input`
  ${styles}
`;

export function Button({ onClick, text, to }) {
  if (onClick) {
    return <Clickable onClick={onClick} value={text} type="button" />;
  }
  return <Component to={to}>{text}</Component>;
}
