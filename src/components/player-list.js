import React from "react";
import styled from "styled-components";

const Table = styled.table`
  width: 100%;
`;

const TableRow = styled.tr`
  background: ${(props) => (props.index % 2 === 0 ? "lightgray" : "white")};
`;
const TableHeader = styled.th`
  width: 80px;
`;
const TableData = styled.td`
  width: 80px;
`;

export function PlayerList(props) {
  return (
    <>
      <Table>
        <thead>
          <TableRow>
            <TableHeader>Nome</TableHeader>
            <TableHeader>Posição</TableHeader>
            <TableHeader>Idade</TableHeader>
            {!props.myClub && <TableHeader>Time</TableHeader>}
          </TableRow>
        </thead>
        <tbody>
          {props.data?.map((item, index) => {
            return (
              <TableRow key={item.id} index={index}>
                <TableData>{item.name}</TableData>
                <TableData>{item.position}</TableData>
                <TableData>{item.age}</TableData>
                {!props.myClub && <TableData>{item.team}</TableData>}
              </TableRow>
            );
          })}
        </tbody>
      </Table>
    </>
  );
}
