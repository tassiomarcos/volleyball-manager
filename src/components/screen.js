import React from "react";
import styled from "styled-components";

const View = styled.div`
  width: 100%;
  height: 780px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export function Screen(props) {
  return <View>{props.children}</View>;
}
