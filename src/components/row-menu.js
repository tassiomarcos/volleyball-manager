import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const Item = styled.div`
  height: 35px;
  color: ${(props) => (props.selected ? "white" : "blue")};
  border-color: blue;
  border-width: 1px;
  border-radius: 2px;
  background: ${(props) => (props.selected ? "blue" : "white")};
  border-right-width: ${(props) => (props.finalIndex ? "1px" : 0)};
  border-top-left-radius: ${(props) => (props.zeroIndex ? "4px" : 0)};
  border-top-right-radius: ${(props) => (props.finalIndex ? "4px" : 0)};
  border-bottom-left-radius: ${(props) => (props.zeroIndex ? "4px" : 0)};
  border-bottom-right-radius: ${(props) => (props.finalIndex ? "4px" : 0)};
  border-style: solid;
  padding: 0;
  margin: 0;
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Text = styled.p`
  font-size: 12px;
`;

export function RowMenu({ data, selected = 0, onClick }) {
  return (
    <Container>
      {data.map((item, index) => {
        return (
          <Item
            onClick={() => onClick(index)}
            selected={selected === index}
            key={index}
            zeroIndex={index === 0}
            finalIndex={index === data.length - 1}
          >
            <Text>{item}</Text>
          </Item>
        );
      })}
    </Container>
  );
}
