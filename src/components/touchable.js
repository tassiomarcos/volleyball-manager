import React from "react";
import styled from "styled-components";
import { GiVolleyballBall } from "react-icons/gi";

const Button = styled.button`
  border: none;
  background: blue;
  height: 45px;
  color: white;
  margin-bottom: 8px;
  outline: none;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 8px;
  font-size: 14px;
  font-weight: bold;
`;

export function Touchable(props) {
  return (
    <Button onClick={props.onClick}>
      <GiVolleyballBall size={16} style={{ marginRight: 12 }} />
      {props.text}
    </Button>
  );
}
