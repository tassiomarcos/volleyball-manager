import React from "react";
import styled from "styled-components";

const Component = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  min-height: 280px;
`;

export function Container(props) {
  return <Component>{props.children}</Component>;
}
