import React from "react";
import styled from "styled-components";

const Component = styled.input`
  height: 45px;
  width: 250px;
  border: 1px solid blue;
  padding-left: 8px;
  border-radius: 4px;
  color: blue;
  font-size: 15px;
  ::placeholder {
    color: blue;
    font-size: 15px;
  }
`;

export function Input(props) {
  return (
    <Component
      type={props.type || "default"}
      onChange={props.onChange}
      placeholder={props.placeholder}
      value={props.value}
    />
  );
}
