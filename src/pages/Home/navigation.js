import React from "react";
import { RowMenu } from "../../components";

const dataNavgation = [
  "Jogadores",
  "Finanças",
  "Escalação",
  "Jogos",
  "Historia",
];

export default function Navigation({ selected, onClick }) {
  return (
    <nav style={{ marginBottom: "24px" }}>
      <RowMenu data={dataNavgation} selected={selected} onClick={onClick} />
    </nav>
  );
}
