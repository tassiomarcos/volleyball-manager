import React, { useEffect, useState } from "react";
import { Screen } from "../../components";
import Body from "./body";
import Menu from "./menu";

import { initGame } from "../../scripts/game";

function ContainerHome(props) {
  return (
    <div
      style={{
        width: "800px",
        height: "780px",
        display: "flex",
      }}
    >
      {props.children}
    </div>
  );
}

export function Home() {
  useEffect(() => {
    initGame();
  }, []);

  return (
    <Screen>
      <ContainerHome>
        <Menu />
        <Body />
      </ContainerHome>
    </Screen>
  );
}
