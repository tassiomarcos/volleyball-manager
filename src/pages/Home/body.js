import React, { useState, useEffect } from "react";
import Header from "./header";
import Navigation from "./navigation";
import Finances from "./finances";
import { PlayerList, Spinner } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { loadGame } from "../../data/game/useCase";
import Games from "./games";

export default function Body() {
  const [showed, setShow] = useState(3);

  const store = useSelector((state) => state.game);
  const dispatch = useDispatch();

  function initGaming() {
    loadGame(dispatch)();
  }

  useEffect(() => {
    initGaming();
  }, []);

  return (
    <div style={{ flex: 3, background: "white" }}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        {store.runningScript && store?.myClub?.players ? (
          <Spinner />
        ) : (
          <>
            <Header name={store?.myClub?.name} />
            <Navigation selected={showed} onClick={setShow} />
            {showed === 0 && (
              <PlayerList data={store?.myClub?.players} myClub />
            )}
            {showed === 1 && <Finances data={store?.myClub?.finance} />}
            {showed === 3 && <Games />}
          </>
        )}
      </div>
    </div>
  );
}
