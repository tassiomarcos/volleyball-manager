import React from "react";

export default function Header(props) {
  return <h1 style={{ color: "blue", margin: "24px" }}>{props.name}</h1>;
}
