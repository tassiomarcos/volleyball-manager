import React, { useState, useEffect } from "react";
import { createRegularChampionship } from "../../scripts/create-championship-table";

export default function Games() {
  const [teams, setTeams] = useState([]);
  useEffect(() => {
    setTeams(createRegularChampionship(["Minas", "Sesc", "Praia", "Osasco"]));
  }, []);

  return (
    <div style={{ width: "100%" }}>
      {teams.length
        ? teams.map((item, index) => {
            return (
              <div key={index} style={{ margin: 0, padding: 0 }}>
                <h4>Rodada {index + 1}</h4>
                {item.map((e, index) => {
                  return (
                    <p kek={index}>
                      {e.mandante} x {e.visitante}
                    </p>
                  );
                })}
              </div>
            );
          })
        : null}
    </div>
  );
}
