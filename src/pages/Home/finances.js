import React from "react";
import styled from "styled-components";

const Table = styled.table`
  width: 80%;
`;

const TableRow = styled.tr`
  background: ${(props) => (props.index % 2 === 0 ? "lightgray" : "white")};
`;
const TableHeader = styled.th`
  width: 80px;
`;
const TableData = styled.td`
  width: 80px;
`;

export default function Finances(props) {
  return props.data ? (
    <Table>
      <TableRow>
        <TableHeader>Info </TableHeader>
        <TableHeader>Valor</TableHeader>
      </TableRow>

      <TableRow index={0}>
        <TableData>Balanço Temporada Anterior</TableData>
        <TableData>${props.data["old-spnonsorship"]}</TableData>
      </TableRow>

      <TableRow>
        <TableData>Patrocinio</TableData>
        <TableData>${props.data.sponsorship}</TableData>
      </TableRow>

      <TableRow index={0}>
        <TableData>Salarios</TableData>
        <TableData>${props.data.payments}</TableData>
      </TableRow>

      <TableRow>
        <TableData>Saldo</TableData>
        <TableData>
          $
          {props.data["old-spnonsorship"] +
            props.data.sponsorship -
            props.data.payments}
        </TableData>
      </TableRow>
    </Table>
  ) : null;
}
