import React from "react";
import { Touchable } from "../../components";
import { useHistory } from "react-router-dom";

const menuData = [
  "Proximo dia",
  "Menu Time",
  "Pesquisar jogadoras",
  "Pesquisar times",
  "Campeonatos",
];

export default function Menu() {
  const history = useHistory();
  return (
    <div
      style={{
        flex: 1,
        display: "flex",
        flexDirection: "column",

        height: "100%",
      }}
    >
      {menuData.map((e) => {
        return (
          <Touchable
            onClick={() => history.push("/create-game")}
            key={e}
            text={e}
          />
        );
      })}
    </div>
  );
}
