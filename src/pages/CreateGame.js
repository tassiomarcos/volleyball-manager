import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Screen, Container, Spinner } from "../components";
// import { initializeDataBase } from "../data/create-game/useCase";
import { useHistory } from "react-router-dom";
import { createGames } from "../data/create-game/useCase";

export function CreateGame(props) {
  const store = useSelector((state) => state.createGame);
  const dispatch = useDispatch();
  const history = useHistory();

  function init() {
    // initializeDataBase(dispatch)();
    createGames();
  }

  function navigateTo() {
    history.push("/home");
  }

  console.log("create");

  useEffect(() => {
    init();
  }, [props]);

  useEffect(() => {
    if (store.success) {
      navigateTo();
    }
  }, [store.success]);

  return (
    <Screen>
      <Container>{store.isLoadingCreateGame && <Spinner />}</Container>
    </Screen>
  );
}
