import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Screen, Selection, Container, Button } from "../components";
import { useHistory } from "react-router-dom";
import { actions } from "../data/create-game";
import {
  getLegueChampionships,
  getClubByChampionshipId,
  addTeam,
} from "../data/create-game/useCase";

export function AddTeamLeague() {
  const [state, setChampionship] = useState(null);
  const [stateClubs] = useState([]);
  const [club, setClub] = useState(null);

  const store = useSelector((state) => state.createGame);
  const dispatch = useDispatch();
  const history = useHistory();

  console.log(store);
  useEffect(() => {
    getLegueChampionships(dispatch)();
  }, []);

  useEffect(() => {
    if (store.championships) {
      setChampionship(store.championships[0]);
      getClubByChampionshipId(dispatch)(store.championships[0].id);
    }
  }, [store.championships]);

  useEffect(() => {
    if (store.clubs) {
      setClub(store.clubs[0]);
    }
  }, [store.clubs]);

  useEffect(() => {
    if (store.success) {
      dispatch({ type: actions.RESET });
      history.push("/create-game");
    }
  }, [store.success]);

  function createTeam() {
    addTeam(dispatch)(club.id);
  }

  return (
    <Screen>
      <Container>
        <Selection
          onChange={(event) => setChampionship(event.target.value)}
          defaultValue={store.championships && store.championships[0]}
          custom={true}
        >
          {store?.championships?.map((e) => {
            return (
              <option key={e.id} value={e}>
                {e.name}
              </option>
            );
          })}
        </Selection>
        <Selection
          custom={true}
          onChange={(event) => setClub(event.target.value)}
        >
          {store?.clubs?.map((e) => {
            return (
              <option key={e.id} value={JSON.stringify(e)}>
                {e.name}
              </option>
            );
          })}
        </Selection>
        <Button onClick={createTeam} text="Criar" />
      </Container>
    </Screen>
  );
}
