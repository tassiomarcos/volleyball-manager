import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Screen, Input, Button, Container } from "../components";
import { countries } from "../data/countries.json";
import { Selection } from "../components/selection";
import { createManager } from "../data/create-game/useCase";
import { useHistory } from "react-router-dom";
import { actions } from "../data/create-game";

export function CreateNewGame() {
  const dispatch = useDispatch();
  const createGame = useSelector((state) => state.createGame);
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [country, setCountry] = useState("");
  const history = useHistory();

  useEffect(() => {
    if (createGame.success) {
      dispatch({ type: actions.RESET });
      history.push("/add-team-league");
    }
  }, [createGame.success]);

  return (
    <Screen>
      <Container>
        <Input
          placeholder="Nome"
          onChange={(event) => setName(event.target.value)}
        />
        <Input
          placeholder="Idade"
          onChange={(event) => setAge(event.target.value)}
          type="number"
        />
        <Selection
          data={countries}
          onChange={(event) => setCountry(event.target.value)}
        />
        <Button
          text="Criar"
          onClick={() => {
            createManager(dispatch)(name, age, country);
          }}
        />
        <Button text="Voltar" to="/" />
      </Container>
    </Screen>
  );
}
