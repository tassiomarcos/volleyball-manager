import React from "react";
import { Screen, Container, Button } from "../components";
import db from "../configs/database";
import { initDatabase, dropTables } from "../data/seeds";

export function InitialPage() {
  initDatabase();
  return (
    <Screen>
      <Container>
        <Button to="/create-new-game" text="Criar Novo" />
        <Button to="/home" text="Carregar" />
        <Button to="/" text="Sair" />
      </Container>
    </Screen>
  );
}
