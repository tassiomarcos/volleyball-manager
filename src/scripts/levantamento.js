import {
  deSegunda,
  bloqueioDuplo,
  bloqueioTriplo,
  bloqueioSimple,
  semBloqueio,
} from "./dispatchs";

export default function choiseBloqueio(incremento, levantadora) {
  let levantamento = levantadora.levantamento + incremento;

  if (levantamento < 1) {
    levantamento = 1;
  }

  if (levantamento > 20) {
    const random = Math.floor(Math.random() * 10 + 1);
    const incremento = Math.floor(Math.random() * 5 + 1);
    switch (random) {
      case 8:
      case 2:
      case 10:
      case 6:
      case 9:
      case 5:
        return {
          ...semBloqueio,
          incremento: 5,
        };
      case 4:
        return { ...bloqueioSimple, incremento };
      case 7:
        return { ...bloqueioTriplo, incremento: -incremento };
      case 1:
        return { ...bloqueioDuplo, incremento: -incremento };
      case 3:
        return { ...deSegunda, incremento };
      default:
        throw new Error(`Invalide Random ${random}`);
    }
  } else {
    const random = Math.floor(Math.random() * 5 + 1);

    switch (levantamento) {
      case 1:
      case 2:
      case 3:
      case 4:
        switch (random) {
          case 1:
          case 2:
          case 3:
            return { ...bloqueioTriplo, incremento: -incremento };
          case 4:
            return { ...bloqueioDuplo, incremento: -incremento };
          case 5:
            return { ...bloqueioSimple, incremento };
          default:
            throw new Error(`Invalide Random ${random}`);
        }
      case 5:
      case 6:
      case 7:
        switch (random) {
          case 1:
          case 2:
            return { ...bloqueioTriplo, incremento: -incremento };
          case 3:
            return { ...bloqueioSimple, incremento };
          case 4:
          case 5:
            return { ...bloqueioDuplo, incremento: -incremento };
          default:
            throw new Error(`Invalide Random ${random}`);
        }
      case 8:
      case 9:
      case 10:
        switch (random) {
          case 1:
          case 2:
            return { ...bloqueioDuplo, incremento: -incremento };
          case 3:
            return { ...bloqueioTriplo, incremento: -incremento };
          case 4:
          case 5:
            return { ...bloqueioSimple, incremento };
          default:
            throw new Error(`Invalide Random ${random}`);
        }
      case 11:
      case 12:
      case 13:
      case 14:
        switch (random) {
          case 1:
            return { ...bloqueioTriplo, incremento: -incremento };
          case 2:
            return { ...bloqueioSimple, incremento };
          case 3:
            return { ...bloqueioDuplo, incremento: -incremento };
          case 4:
            return { ...deSegunda };
          case 5:
            return { ...semBloqueio, incremento: 5 };
          default:
            throw new Error(`Invalide Random ${random}`);
        }
      case 15:
      case 16:
      case 17:
        switch (random) {
          case 1:
            return { ...bloqueioSimple, incremento };
          case 2:
            return { ...bloqueioDuplo, incremento: -incremento };
          case 3:
          case 4:
            return { ...semBloqueio, incremento: 5 };
          case 5:
            return { ...deSegunda };
          default:
            throw new Error(`Invalide Random ${random}`);
        }
      case 18:
      case 19:
        switch (random) {
          case 1:
          case 2:
          case 3:
            return { ...bloqueioSimple, incremento };
          case 4:
            return { ...bloqueioDuplo, incremento: -incremento };
          case 5:
            return { ...deSegunda };
          default:
            throw new Error(`Invalide Random ${random}`);
        }
      case 20:
        switch (random) {
          case 1:
          case 2:
          case 3:
          case 4:
            return { ...bloqueioSimple };
          case 5:
            return { ...deSegunda };
          default:
            throw new Error(`Invalide Random ${random}`);
        }
      default:
        break;
    }
  }
}
