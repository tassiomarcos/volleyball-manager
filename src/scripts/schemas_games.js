const taticGame = {
  rapido: "rapido",
  saida: "saida",
  entrada: "entrada",
  equilibrado: "equilibrado",
};

const taticAttack = {
  forte: "forte",
  tecnico: "tecnico",
};

export default {
  taticAttack,
  taticGame,
};
