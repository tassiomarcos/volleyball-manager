import recepcao from "./recepcao";
import ataque from "./ataque";
import defesa from "./defesa";
import { endSet, endGame } from "./utils";
import actions from "./actions";
import schemas_games from "./schemas_games";

const mandante = "Praia Clube";
const visitante = "Sesc";

export function initGame() {
  try {
    const msacador = { saque: 20 };
    const mrecep1 = { passe: 14, defesa: 16 };
    const mrecep2 = { passe: 18, defesa: 19 };
    const mrecep3 = { passe: 16, defesa: 14 };

    const mblk1 = { bloqueio: 15 };
    const mblk2 = { bloqueio: 20 };
    const mblk3 = { bloqueio: 15 };

    const matk1 = { forca: 20, tecnica: 19 };
    const matk2 = { forca: 19, tecnica: 17 };
    const matk3 = { forca: 17, tecnica: 16 };
    const matk4 = { forca: 18, tecnica: 10 };

    const mlev = {
      forca: 13,
      tecnica: 10,
      levantamento: 20,
      criatividade: 18,
      posicao: "levantadora",
    };

    const vsacador = { saque: 1 };
    const vrecep1 = { passe: 1, defesa: 16 };
    const vrecep2 = { passe: 1, defesa: 15 };
    const vrecep3 = { passe: 1, defesa: 14 };

    const vblk1 = { bloqueio: 15 };
    const vblk2 = { bloqueio: 12 };
    const vblk3 = { bloqueio: 14 };

    const vatk1 = { forca: 2, tecnica: 12 };
    const vatk2 = { forca: 5, tecnica: 11 };
    const vatk3 = { forca: 4, tecnica: 10 };
    const vatk4 = { forca: 3, tecnica: 9 };

    const vlev = {
      forca: 13,
      tecnica: 10,
      levantamento: 5,
      criatividade: 18,
      posicao: "levantadora",
    };

    let timeMandante = 0;
    let timeVisitante = 0;
    let timeMandanteSet = 0;
    let timeVisitanteSet = 0;
    let timeNoSaque = mandante;
    let eSaque = true;

    const sets = [];

    while (!endGame(timeMandanteSet, timeVisitanteSet)) {
      let setsJogados = timeMandanteSet + timeVisitanteSet;
      setsJogados % 2 === 0
        ? (timeNoSaque = mandante)
        : (timeNoSaque = visitante);

      while (endSet(timeMandante, timeVisitante)) {
        if (timeNoSaque === mandante) {
          console.log(mandante);
          const jogada = ponto(
            mandante,
            visitante,
            timeMandante,
            timeVisitante,
            eSaque,
            msacador,
            vrecep1,
            vrecep2,
            vrecep3,
            schemas_games.taticGame.equilibrado,
            schemas_games.taticAttack.tecnico,
            vlev,
            vatk1,
            vatk2,
            vatk3,
            vatk4,
            mblk1,
            mblk2,
            mblk3,
            mrecep1,
            mrecep2,
            mrecep3
          );
          timeMandante = jogada.timeSaquePonto;
          timeVisitante = jogada.timeAtaquePonto;
          timeNoSaque = jogada.vaiProSaque;
          eSaque = jogada.eSaque;
        }
        if (timeNoSaque === visitante) {
          console.log(visitante);
          let jogada = ponto(
            visitante,
            mandante,
            timeVisitante,
            timeMandante,
            eSaque,
            vsacador,
            mrecep1,
            mrecep2,
            mrecep3,
            schemas_games.taticGame.rapido,
            schemas_games.taticAttack.tecnico,
            mlev,
            matk1,
            matk2,
            matk3,
            matk4,
            vblk1,
            vblk2,
            vblk3,
            vrecep1,
            vrecep2,
            vrecep3
          );
          timeMandante = jogada.timeAtaquePonto;
          timeVisitante = jogada.timeSaquePonto;
          timeNoSaque = jogada.vaiProSaque;
          eSaque = jogada.eSaque;
        }

        console.log(
          `${mandante}: ${timeMandante} x ${timeVisitante} :${visitante}`
        );
      }

      if (timeMandante > timeVisitante) {
        timeMandanteSet += 1;
      } else {
        timeVisitanteSet += 1;
      }

      sets.push({ [mandante]: timeMandante, [visitante]: timeVisitante });
      timeMandante = 0;
      timeVisitante = 0;
      console.log(sets);
    }
  } catch (e) {
    console.log(e);
  }
}

function ponto(
  timeSaque,
  timeAtaque,
  timeSaquePonto,
  timeAtaquePonto,
  eSaque,
  sacador,
  recepcao1,
  recepcao2,
  recepcao3,
  taticaLevantamento,
  taticaAtaque,
  levantador,
  oposto,
  pontaEntrada,
  central,
  pontaFundo,
  entradaBloque,
  centralBloque,
  saidaBloque,
  defesa1,
  defesa2,
  defesa3
) {
  try {
    const timeAtaqueFezPonto = {
      timeAtaquePonto: timeAtaquePonto + 1,
      timeSaquePonto,
      vaiProSaque: timeAtaque,
      eSaque: true,
    };
    const timeSaqueFezPonto = {
      timeAtaquePonto,
      timeSaquePonto: timeSaquePonto + 1,
      vaiProSaque: timeSaque,
      eSaque: true,
    };

    const naoFezPonto = {
      timeAtaquePonto,
      timeSaquePonto,
      vaiProSaque: timeAtaque,
      eSaque: false,
    };

    let acaoSaque;
    let acaoAtaque;
    let acaoDefesa;

    if (eSaque) {
      acaoSaque = recepcao(sacador, recepcao1, recepcao2, recepcao3);
      console.log(acaoSaque);
    }

    if (acaoSaque) {
      if (acaoSaque.endOfPoint) {
        if (acaoSaque.type === actions.erro_recepcao) {
          return timeSaqueFezPonto;
        }

        if (acaoSaque.type === actions.erro_saque) {
          return timeAtaqueFezPonto;
        }
      } else {
        acaoAtaque = ataque(
          taticaLevantamento,
          taticaAtaque,
          acaoSaque.incremento,
          levantador,
          oposto,
          pontaEntrada,
          central,
          pontaFundo,
          entradaBloque,
          centralBloque,
          saidaBloque
        );

        console.log(acaoAtaque);

        if (acaoAtaque.endOfPoint) {
          if (acaoAtaque.type === actions.bloqueio_efetivado) {
            return timeSaqueFezPonto;
          }
          if (acaoAtaque.type === actions.bloqueio_pra_fora) {
            return timeAtaqueFezPonto;
          }
          if (acaoAtaque.type === actions.direto_prafora) {
            return timeSaqueFezPonto;
          }
          if (acaoAtaque.type === actions.bloqueio_antena) {
            return timeAtaqueFezPonto;
          }
        } else {
          if (acaoAtaque.type !== actions.de_segunda) {
            acaoDefesa = defesa(
              defesa1,
              defesa2,
              defesa3,
              acaoAtaque.atacante,
              acaoAtaque.incremento,
              taticaAtaque
            );
          }

          if (acaoAtaque.type === actions.de_segunda) {
            acaoDefesa = defesa(
              defesa1,
              defesa2,
              defesa3,
              levantador,
              acaoAtaque.incremento,
              taticaAtaque
            );
          }

          console.log(acaoDefesa);
          if (acaoDefesa.endOfPoint) {
            if (acaoDefesa.type === actions.defesaPraFora) {
              return timeAtaqueFezPonto;
            }
            if (acaoDefesa.type === actions.praFora) {
              return timeSaqueFezPonto;
            }
            if (acaoDefesa.type === actions.ataqueDireto) {
              return timeAtaqueFezPonto;
            }
          } else {
            return naoFezPonto;
          }
        }
      }
    } else {
      acaoAtaque = ataque(
        taticaLevantamento,
        taticaAtaque,
        0,
        levantador,
        oposto,
        pontaEntrada,
        central,
        pontaFundo,
        entradaBloque,
        centralBloque,
        saidaBloque
      );

      console.log(acaoAtaque);

      if (acaoAtaque.endOfPoint) {
        if (acaoAtaque.type === actions.bloqueio_efetivado) {
          return timeSaqueFezPonto;
        }
        if (acaoAtaque.type === actions.bloqueio_pra_fora) {
          return timeAtaqueFezPonto;
        }
        if (acaoAtaque.type === actions.direto_prafora) {
          return timeSaqueFezPonto;
        }
        if (acaoAtaque.type === actions.bloqueio_antena) {
          return timeAtaqueFezPonto;
        }
      } else {
        if (acaoAtaque.type !== actions.de_segunda) {
          acaoDefesa = defesa(
            defesa1,
            defesa2,
            defesa3,
            acaoAtaque.atacante,
            acaoAtaque.incremento,
            taticaAtaque
          );
        }
        if (acaoAtaque.type === actions.de_segunda) {
          acaoDefesa = defesa(
            defesa1,
            defesa2,
            defesa3,
            levantador,
            acaoAtaque.incremento,
            taticaAtaque
          );
        }

        console.log(acaoDefesa);

        if (acaoDefesa.endOfPoint) {
          if (acaoDefesa.type === actions.defesaPraFora) {
            return timeAtaqueFezPonto;
          }
          if (acaoDefesa.type === actions.praFora) {
            return timeSaqueFezPonto;
          }
          if (acaoDefesa.type === actions.ataqueDireto) {
            return timeAtaqueFezPonto;
          }
        } else {
          return naoFezPonto;
        }
      }
    }
  } catch (e) {
    throw e;
  }
}
