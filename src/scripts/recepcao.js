import utils from "./utils";

import {
  erroDeSaque,
  erroDeRecepcao,
  recepcaoNormal,
  recepcaoQuebrada,
  recepcaoPerfeita,
} from "./dispatchs";

export default function (saque, recepcao1, recepcao2, recepcao3) {
  const position = Math.floor(Math.random() * 3 + 1);
  const random = Math.floor(Math.random() * 10 + 1);

  let playerRecp;

  if (position === 1) playerRecp = recepcao1;
  if (position === 2) playerRecp = recepcao2;
  if (position === 3) playerRecp = recepcao3;

  if (saque.saque < playerRecp.passe) {
    const value = utils.returnDiference(playerRecp.passe, saque.saque);
    switch (value) {
      case 1:
        switch (random) {
          case 8:
          case 1:
            return recepcaoNormal;
          case 6:
          case 2:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 10:
          case 4:
            return erroDeRecepcao;
          case 7:
          case 5:
            return recepcaoQuebrada;
        }
      case 2:
        switch (random) {
          case 8:
          case 1:
          case 6:
            return recepcaoNormal;
          case 2:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 10:
          case 4:
            return erroDeRecepcao;
          case 7:
          case 5:
            return recepcaoQuebrada;
        }
      case 3:
        switch (random) {
          case 8:
          case 1:
          case 10:
          case 7:
          case 2:
            return recepcaoNormal;
          case 6:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 5:
            return recepcaoQuebrada;
        }
      case 4:
        switch (random) {
          case 8:
          case 1:
          case 7:
          case 6:
            return recepcaoNormal;
          case 2:
          case 10:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 5:
            return recepcaoQuebrada;
        }
      case 5:
        switch (random) {
          case 8:
          case 1:
          case 6:
          case 5:
            return recepcaoNormal;
          case 10:
          case 2:
          case 3:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 7:
            return recepcaoQuebrada;
        }
      case 6:
        switch (random) {
          case 8:
          case 1:
          case 6:
            return recepcaoNormal;
          case 5:
          case 10:
          case 2:
          case 3:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 7:
            return recepcaoQuebrada;
        }
      case 7:
      case 8:
        switch (random) {
          case 8:
          case 1:
          case 6:
            return recepcaoNormal;
          case 5:
          case 10:
          case 2:
          case 3:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 7:
            return recepcaoQuebrada;
        }
      case 9:
        switch (random) {
          case 8:
          case 1:
          case 6:
            return recepcaoNormal;
          case 5:
          case 10:
          case 2:
          case 3:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 7:
            return recepcaoQuebrada;
        }
      case 10:
      case 12:
      case 11:
        switch (random) {
          case 8:
          case 1:
            return recepcaoNormal;
          case 6:
          case 5:
          case 10:
          case 2:
          case 3:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 7:
            return recepcaoQuebrada;
        }
      case 13:
      case 15:
      case 14:
      case 16:
      case 17:
        switch (random) {
          case 8:
          case 1:
            return recepcaoNormal;
          case 6:
          case 5:
          case 10:
          case 2:
          case 3:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 7:
            return recepcaoQuebrada;
        }
      case 18:
      case 19:
        switch (random) {
          case 8:
            return recepcaoNormal;
          case 1:
          case 6:
          case 5:
          case 10:
          case 2:
          case 3:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 7:
            return recepcaoQuebrada;
        }
      case 20:
        switch (random) {
          case 8:
          case 1:
          case 6:
          case 5:
          case 10:
          case 2:
          case 3:
          case 4:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 7:
            return recepcaoQuebrada;
        }
    }
  }

  if (saque.saque > playerRecp.passe) {
    const value = utils.returnDiference(playerRecp.passe, saque.saque);

    switch (value) {
      case 1:
        switch (random) {
          case 8:
          case 1:
            return recepcaoNormal;
          case 6:
          case 2:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 10:
          case 4:
            return erroDeRecepcao;
          case 7:
          case 5:
            return recepcaoQuebrada;
        }
      case 2:
        switch (random) {
          case 8:
          case 1:
            return recepcaoNormal;
          case 2:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 10:
          case 4:
            return erroDeRecepcao;
          case 7:
          case 6:
          case 5:
            return recepcaoQuebrada;
        }
      case 3:
        switch (random) {
          case 8:
          case 1:
          case 10:
          case 7:
            return recepcaoNormal;
          case 6:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 2:
          case 5:
            return recepcaoQuebrada;
        }
      case 4:
        switch (random) {
          case 8:
          case 1:
          case 7:
            return recepcaoNormal;
          case 2:
          case 10:
            return recepcaoPerfeita;
          case 9:
          case 3:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 6:
          case 5:
            return recepcaoQuebrada;
        }
      case 5:
      case 6:
        switch (random) {
          case 8:
          case 1:
          case 6:
            return recepcaoNormal;
          case 10:
          case 2:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 4:
            return erroDeRecepcao;
          case 5:
          case 3:
          case 7:
            return recepcaoQuebrada;
        }

      case 7:
      case 8:
        switch (random) {
          case 8:
          case 1:
          case 6:
            return recepcaoNormal;
          case 5:
          case 10:
            return recepcaoQuebrada;
          case 3:
          case 9:
            return erroDeSaque;
          case 4:
          case 2:
            return erroDeRecepcao;
          case 7:
            return recepcaoPerfeita;
        }
      case 9:
        switch (random) {
          case 8:
          case 1:
            return recepcaoNormal;
          case 5:
            return recepcaoPerfeita;
          case 9:
            return erroDeSaque;
          case 10:
          case 3:
          case 4:
            return erroDeRecepcao;
          case 2:
          case 6:
          case 7:
            return recepcaoQuebrada;
        }
      case 10:
      case 12:
      case 11:
        switch (random) {
          case 8:
          case 1:
            return erroDeRecepcao;
          case 6:
          case 5:
          case 10:
          case 2:
          case 3:
            return recepcaoQuebrada;
          case 9:
            return erroDeSaque;
          case 4:
            return recepcaoPerfeita;
          case 7:
            return recepcaoNormal;
        }
      case 13:
      case 15:
      case 14:
      case 16:
      case 17:
        switch (random) {
          case 8:
            return recepcaoNormal;
          case 1:
          case 4:
          case 6:
          case 5:
          case 10:
            return erroDeRecepcao;
          case 2:
          case 3:
            return recepcaoQuebrada;
          case 9:
            return erroDeSaque;
          case 7:
            return recepcaoPerfeita;
        }
      case 18:
      case 19:
        switch (random) {
          case 8:
          case 1:
          case 6:
          case 5:
          case 10:
          case 2:
            return erroDeRecepcao;
          case 3:
            return recepcaoNormal;
          case 9:
            return erroDeSaque;
          case 7:
            return recepcaoQuebrada;
        }
      case 20:
        switch (random) {
          case 8:
          case 1:
          case 6:
          case 5:
          case 10:
          case 2:
          case 3:
          case 4:
            return erroDeRecepcao;
          case 9:
            return erroDeSaque;
          case 7:
            return recepcaoQuebrada;
        }
    }
  }
  if (saque.saque === playerRecp.passe) {
    switch (random) {
      case 8:
      case 1:
        return recepcaoNormal;
      case 6:
      case 2:
        return recepcaoPerfeita;
      case 9:
      case 3:
        return erroDeSaque;
      case 10:
      case 4:
        return erroDeRecepcao;
      case 7:
      case 5:
        return recepcaoQuebrada;
    }
  }
}
