import { defesaPraFora, praFora, ataqueDireto, defesa } from "./dispatchs";
import schemas_games from "./schemas_games";
import utils from "./utils";

export default function (
  defesa1,
  defesa2,
  defesa3,
  atacante,
  incrementoAtaque,
  taticaAtaque
) {
  const defesaRandom = Math.floor(Math.random() * 3 + 1);
  let defesaPlayer;

  const ataque =
    taticaAtaque === schemas_games.taticAttack.forte
      ? atacante.forca + incrementoAtaque
      : atacante.tecnica + incrementoAtaque;

  if (defesaRandom === 1) defesaPlayer = defesa1;
  if (defesaRandom === 2) defesaPlayer = defesa2;
  if (defesaRandom === 3) defesaPlayer = defesa3;

  const value = utils.returnDiference(defesaPlayer.defesa, ataque);

  // if (defesaPlayer.defesa > ataque) {
  //   const random = Math.floor(Math.random() * 4 + 1);
  //   switch (value) {
  //     case 1:
  //     case 2:
  //     case 3:
  //     case 4:
  //     case 5:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 6:
  //     case 7:
  //     case 8:
  //     case 9:
  //     case 10:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 11:
  //     case 12:
  //     case 13:
  //     case 14:
  //     case 15:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 16:
  //     case 17:
  //     case 18:
  //     case 19:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 20:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     default:
  //       break;
  //   }
  // }

  // // ataque masi forte
  // if (defesaPlayer.defesa < ataque) {
  //   const random = Math.floor(Math.random() * 10 + 1);
  //   switch (value) {
  //     case 1:
  //     case 2:
  //     case 3:
  //     case 4:
  //     case 5:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         case 5:
  //         case 6:
  //         case 7:
  //         case 8:
  //         case 9:
  //         case 10:

  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 6:
  //     case 7:
  //     case 8:
  //     case 9:
  //     case 10:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         case 5:
  //         case 6:
  //         case 7:
  //         case 8:
  //         case 9:
  //         case 10:
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 11:
  //     case 12:
  //     case 13:
  //     case 14:
  //     case 15:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         case 5:
  //         case 6:
  //         case 7:
  //         case 8:
  //         case 9:
  //         case 10:
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 16:
  //     case 17:
  //     case 18:
  //     case 19:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         case 5:
  //         case 6:
  //         case 7:
  //         case 8:
  //         case 9:
  //         case 10:
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     case 20:
  //       switch (random) {
  //         case 1:
  //           return defesa;
  //         case 2:
  //           return praFora;
  //         case 3:
  //           return defesaPraFora;
  //         case 4:
  //           return ataqueDireto;
  //         case 5:
  //         case 6:
  //         case 7:
  //         case 8:
  //         case 9:
  //         case 10:
  //         default:
  //           throw new Error("Invalide Random");
  //       }
  //     default:
  //       break;
  //   }
  // }

  // if (defesaPlayer === ataque) {
  const random = Math.floor(Math.random() * 4 + 1);
  switch (random) {
    case 1:
      return defesa;
    case 2:
      return praFora;
    case 3:
      return defesaPraFora;
    case 4:
      return ataqueDireto;
    default:
      throw new Error("Invalide Random");
  }
  // }
}
