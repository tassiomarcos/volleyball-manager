import schemas_games from "./schemas_games";

export default function (oposto, pontaEntrada, central, pontaFundo, tatica) {
  const random = Math.floor(Math.random() * 6 + 1);

  if (tatica === schemas_games.taticGame.rapido) {
    switch (random) {
      case 8:
      case 2:
      case 3:
      case 6:
      case 5:
        return central;
      case 1:
        return oposto;
      case 7:
        return pontaEntrada;
      case 4:
        return pontaFundo;
    }
  }
  if (tatica === schemas_games.taticGame.saida) {
    switch (random) {
      case 8:
      case 2:
      case 3:
      case 6:
      case 5:
        return oposto;
      case 1:
        return central;
      case 4:
        return pontaEntrada;
      case 7:
        return pontaFundo;
    }
  }
  if (tatica === schemas_games.taticGame.entrada) {
    switch (random) {
      case 8:
      case 2:
      case 3:
      case 6:
      case 5:
        return pontaEntrada;
      case 1:
        return pontaFundo;
      case 4:
        return central;
      case 7:
        return oposto;
    }
  }
  if (tatica === schemas_games.taticGame.equilibrado) {
    switch (random) {
      case 8:
      case 2:
        return oposto;
      case 3:
      case 6:
        return central;
      case 5:
      case 1:
        return pontaEntrada;
      case 4:
      case 7:
        return pontaFundo;
    }
  }
}
