function isStrong(time1, time2) {
  if (time1.position < time2.position) {
    return true;
  } else {
    return false;
  }
}

function isWin(score1, score2) {
  if (score1 > score2) {
    return true;
  }

  return false;
}

function sumScores(value1, value2) {
  if (value1 > value2) {
    return (value1 - value2) * 0.01;
  }

  return (value2 - value1) * 0.01;
}

function endSet(value1, value2) {
  let sum;

  if (value1 > value2) {
    sum = value1 - value2;
  } else {
    sum = value2 - value1;
  }

  return !((value1 > 24 || value2 > 24) && sum >= 2);
}

function endGame(setTime1, setTime2) {
  return setTime1 === 3 || setTime2 === 3;
}

function returnDiference(force1, force2) {
  if (force1 > force2) {
    return force1 - force2;
  }

  return force2 - force1;
}

export default { isStrong, isWin, sumScores, endSet, endGame, returnDiference };
export { isStrong, isWin, sumScores, endSet, endGame, returnDiference };
