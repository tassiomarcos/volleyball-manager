import actions from "./actions";

const amorteceuNoBloqueio = {
  endOfPoint: false,
  type: actions.amorteceu,
};
const bloqueioEfetivado = {
  endOfPoint: true,
  type: actions.bloqueio_efetivado,
  incremento: 0,
};
const bloqueioPraFora = {
  endOfPoint: true,
  type: actions.bloqueio_pra_fora,
  incremento: 0,
};
const bloqueioAntena = {
  endOfPoint: true,
  type: actions.bloqueio_antena,
  incremento: 0,
};
const diretoPraFora = {
  endOfPoint: true,
  type: actions.direto_prafora,
  incremento: 0,
};
const porCimaDoBloqueio = {
  endOfPoint: false,
  type: actions.por_cima_do_bloqueio,
};

const erroDeSaque = {
  endOfPoint: true,
  incremento: 0,
  type: actions.erro_saque,
};

const erroDeRecepcao = {
  endOfPoint: true,
  incremento: 0,
  type: actions.erro_recepcao,
};

const recepcaoNormal = {
  endOfPoint: false,
  incremento: 0,
  type: actions.recepcao_normal,
};

const recepcaoQuebrada = {
  endOfPoint: false,
  incremento: -5,
  type: actions.recepcao_quebrada,
};

const recepcaoPerfeita = {
  endOfPoint: false,
  incremento: 5,
  type: actions.recepcao_perfeita,
};

const semBloqueio = {
  endOfPoint: false,
  type: actions.sem_bloqueio,
};

const bloqueioSimple = {
  endOfPoint: false,
  type: actions.bloqueio_simple,
};

const bloqueioDuplo = {
  endOfPoint: false,
  type: actions.bloqueio_duplo,
};

const bloqueioTriplo = {
  endOfPoint: false,
  type: actions.bloqueio_triplo,
};

const deSegunda = {
  endOfPoint: false,
  type: actions.de_segunda,
};

const defesaPraFora = {
  endOfPoint: true,
  type: actions.defesaPraFora,
};

const praFora = {
  endOfPoint: true,
  type: actions.praFora,
};

const ataqueDireto = {
  endOfPoint: true,
  type: actions.ataqueDireto,
};

const defesa = {
  endOfPoint: false,
  type: actions.defesa,
};

export {
  //defesa
  defesaPraFora,
  praFora,
  ataqueDireto,
  defesa,
  //levantamento
  amorteceuNoBloqueio,
  bloqueioEfetivado,
  bloqueioPraFora,
  bloqueioAntena,
  diretoPraFora,
  porCimaDoBloqueio,
  // recepcao
  erroDeSaque,
  erroDeRecepcao,
  recepcaoNormal,
  recepcaoQuebrada,
  recepcaoPerfeita,
  //bloqueio - recepcao
  deSegunda,
  bloqueioDuplo,
  bloqueioTriplo,
  bloqueioSimple,
  semBloqueio,
};
