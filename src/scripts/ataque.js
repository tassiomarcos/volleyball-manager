import ChooseAtacante from "./tatica-ataque";
import LevantamentoType from "./levantamento";
import Bloqueio from "./bloqueio";
import actions from "./actions";

export default function (
  taticaLevantametno,
  taticaAtaque,
  incrementoLevantamento,
  levantadora,
  oposto,
  pontaEntrada,
  central,
  pontaFundo,
  entradaBloque,
  centralBloque,
  saidaBloque
) {
  const atacante = ChooseAtacante(
    oposto,
    pontaEntrada,
    central,
    pontaFundo,
    taticaLevantametno
  );

  const levantamentoType = LevantamentoType(
    incrementoLevantamento,
    levantadora
  );

  if (levantamentoType.type === actions.bloqueio_triplo) {
    let bloqueio = Bloqueio(
      entradaBloque,
      saidaBloque,
      centralBloque,
      atacante,
      taticaAtaque,
      levantamentoType.incremento,
      5
    );

    return { ...bloqueio, atacante };
  }

  if (levantamentoType.type === actions.bloqueio_simple) {
    let bloqueio = Bloqueio(
      entradaBloque,
      saidaBloque,
      centralBloque,
      atacante,
      taticaAtaque,
      levantamentoType.incremento,
      -3
    );

    return { ...bloqueio, atacante };
  }

  if (levantamentoType.type === actions.bloqueio_duplo) {
    let bloqueio = Bloqueio(
      entradaBloque,
      saidaBloque,
      centralBloque,
      atacante,
      taticaAtaque,
      levantamentoType.incremento,
      2
    );

    return { ...bloqueio, atacante };
  }

  return { ...levantamentoType, atacante };
}
