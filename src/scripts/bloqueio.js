import schema_game from "./schemas_games";
import {
  amorteceuNoBloqueio,
  bloqueioEfetivado,
  bloqueioPraFora,
  bloqueioAntena,
  diretoPraFora,
  porCimaDoBloqueio,
} from "./dispatchs";

export default function (
  bloqueioEntrada,
  bloqueioSaida,
  bloqueioCentro,
  atacante,
  tipoAtaque,
  incrementoAtaque = 0,
  incrementoBloqueio = 0,
  type
) {
  let ataque;
  let bloqueio;
  let random = Math.floor(Math.random() * 12 + 1);

  if (tipoAtaque === schema_game) {
    ataque = atacante.tecnica + incrementoAtaque;
  }
  if (tipoAtaque === "forte") {
    ataque = atacante.forca + incrementoAtaque;
  }

  if (type === "bloqueio_simples") {
    bloqueio = bloqueioCentro.bloqueio + incrementoBloqueio;

    if (bloqueio > ataque) {
      let valor = bloqueio - ataque;
      bloqueioMaisForte(random, valor);
    }
    if (bloqueio < ataque) {
      let valor = ataque - bloqueio;
      ataqueMaisForte(random, valor);
    }
    if (bloqueio === ataque) {
      return ataqueIgualBloqueio();
    }
  }

  if (type === "bloqueio_duplo") {
    bloqueio =
      (bloqueioCentro.bloqueio + bloqueioSaida.bloqueio) / 2 +
      incrementoBloqueio;

    if (bloqueio > ataque) {
      let valor = bloqueio - ataque;
      bloqueioMaisForte(random, valor);
    }
    if (bloqueio < ataque) {
      let valor = ataque - bloqueio;
      ataqueMaisForte(random, valor);
    }
    if (bloqueio === ataque) {
      return ataqueIgualBloqueio();
    }
  }

  if (type === "bloqueio_triplo") {
    bloqueio =
      (bloqueioCentro.bloqueio +
        bloqueioSaida.bloqueio +
        bloqueioEntrada.bloqueio) /
        3 +
      incrementoBloqueio;

    if (bloqueio > ataque) {
      let valor = bloqueio - ataque;
      bloqueioMaisForte(random, valor);
    }
    if (bloqueio < ataque) {
      let valor = ataque - bloqueio;
      ataqueMaisForte(random, valor);
    }
    if (bloqueio === ataque) {
      return ataqueIgualBloqueio();
    }
  }
}

function ataqueMaisForte(random, valor) {
  const incremento = Math.floor(Math.random * 5 + 1);

  let amoterceu = { ...amorteceuNoBloqueio, incremento: -incremento };
  let porCimaDoBloque = { ...porCimaDoBloqueio, incremento };

  switch (valor) {
    case 1:
    case 2:
    case 3:
    case 4:
      switch (random) {
        case 1:
        case 5:
          return diretoPraFora;
        case 2:
        case 3:
          return bloqueioEfetivado;
        case 4:
        case 6:
          return amoterceu;
        case 7:
        case 8:
          return bloqueioPraFora;
        case 9:
        case 10:
          return porCimaDoBloque;
        case 11:
        case 12:
          return bloqueioAntena;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 5:
    case 6:
    case 7:
    case 8:
      switch (random) {
        case 1:
          return diretoPraFora;
        case 5:
        case 3:
          return bloqueioEfetivado;
        case 4:
        case 10:
          return amoterceu;
        case 7:
        case 6:
        case 2:
          return bloqueioPraFora;
        case 8:
        case 11:
          return porCimaDoBloque;
        case 12:
        case 9:
          return bloqueioAntena;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 9:
    case 10:
    case 11:
      switch (random) {
        case 1:
          return diretoPraFora;
        case 5:
          return bloqueioEfetivado;
        case 2:
        case 3:
          return amoterceu;
        case 4:
        case 6:
        case 7:
          return bloqueioPraFora;
        case 8:
        case 9:
        case 10:
          return porCimaDoBloque;
        case 11:
        case 12:
          return bloqueioAntena;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 12:
    case 13:
    case 14:
      switch (random) {
        case 1:
        case 5:
          return diretoPraFora;
        case 2:
        case 3:
          return bloqueioEfetivado;
        case 4:
        case 6:
          return amoterceu;
        case 7:
        case 8:
          return bloqueioPraFora;
        case 9:
        case 10:
          return porCimaDoBloque;
        case 11:
        case 12:
          return bloqueioAntena;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 15:
    case 16:
      switch (random) {
        case 5:
          return diretoPraFora;
        case 1:
          return bloqueioAntena;
        case 2:
          return bloqueioEfetivado;
        case 3:
        case 12:
          return amoterceu;
        case 4:
        case 6:
        case 7:
        case 8:
          return bloqueioPraFora;
        case 9:
        case 11:
        case 10:
          return porCimaDoBloque;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 17:
    case 18:
    case 19:
      switch (random) {
        case 5:
          return diretoPraFora;
        case 2:
          return bloqueioEfetivado;
        case 3:
        case 4:
          return bloqueioAntena;
        case 1:
        case 6:
          return amoterceu;
        case 7:
        case 8:
        case 9:
          return bloqueioPraFora;
        case 10:
        case 11:
        case 12:
          return porCimaDoBloque;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 20:
      switch (random) {
        case 8:
          return diretoPraFora;
        case 9:
          return bloqueioEfetivado;
        case 2:
          return amoterceu;
        case 12:
          return bloqueioPraFora;
        case 4:
          return bloqueioAntena;
        case 6:
        case 7:
        case 1:
        case 3:
        case 10:
        case 5:
        case 11:
          return porCimaDoBloque;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    default:
      break;
  }
}
function bloqueioMaisForte(random, valor) {
  const incremento = Math.floor(Math.random * 5 + 1);

  let amoterceu = { ...amorteceuNoBloqueio, incremento: -incremento };
  let porCimaDoBloque = { ...porCimaDoBloqueio, incremento };

  switch (valor) {
    case 1:
    case 2:
    case 3:
    case 4:
      switch (random) {
        case 1:
        case 5:
          return diretoPraFora;
        case 2:
        case 3:
          return bloqueioEfetivado;
        case 4:
        case 6:
          return amoterceu;
        case 7:
        case 8:
          return bloqueioPraFora;
        case 9:
        case 10:
          return porCimaDoBloque;
        case 11:
        case 12:
          return bloqueioAntena;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 5:
    case 6:
    case 7:
    case 8:
      switch (random) {
        case 1:
          return porCimaDoBloque;
        case 5:
        case 3:
          return bloqueioPraFora;
        case 4:
        case 10:
          return amoterceu;
        case 7:
        case 6:
        case 2:
          return bloqueioEfetivado;
        case 8:
        case 11:
        case 12:
          return diretoPraFora;
        case 9:
          return bloqueioAntena;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 9:
    case 10:
    case 11:
      switch (random) {
        case 1:
          return porCimaDoBloque;
        case 5:
        case 2:
          return diretoPraFora;
        case 3:
        case 4:
        case 6:
          return amoterceu;
        case 7:
          return bloqueioPraFora;
        case 8:
          return bloqueioAntena;
        case 9:
        case 10:
        case 11:
        case 12:
          return bloqueioEfetivado;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 12:
    case 13:
    case 14:
      switch (random) {
        case 5:
        case 1:
        case 6:
          return diretoPraFora;
        case 2:
        case 3:
        case 4:
        case 9:
        case 10:
          return bloqueioEfetivado;
        case 7:
        case 8:
          return amoterceu;
        case 11:
          return porCimaDoBloque;
        case 12:
          return bloqueioAntena;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 15:
    case 16:
      switch (random) {
        case 5:
          return diretoPraFora;
        case 1:
          return bloqueioAntena;
        case 2:
          return bloqueioPraFora;
        case 4:
          return porCimaDoBloque;
        case 6:
        case 7:
        case 8:
          return amoterceu;
        case 3:
        case 12:
        case 9:
        case 11:
        case 10:
          return bloqueioEfetivado;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 17:
    case 18:
    case 19:
      switch (random) {
        case 5:
          return diretoPraFora;
        case 2:
        case 10:
        case 3:
        case 9:
        case 12:
          return bloqueioEfetivado;
        case 4:
          return bloqueioAntena;
        case 1:
        case 6:
        case 7:
        case 11:
          return amoterceu;
        case 8:
          return bloqueioPraFora;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    case 20:
      switch (random) {
        case 8:
        case 2:
        case 9:
          return amoterceu;
        case 12:
          return bloqueioPraFora;
        case 4:
          return bloqueioAntena;
        case 6:
        case 7:
        case 1:
        case 3:
        case 10:
        case 5:
        case 11:
          return bloqueioEfetivado;
        default:
          throw new Error(`Invalide Random ${random}`);
      }
    default:
      break;
  }
}
function ataqueIgualBloqueio() {
  const random = Math.floor(Math.random * 6 + 1);
  const incremento = Math.floor(Math.random * 5 + 1);
  switch (random) {
    case 1:
      return { ...amorteceuNoBloqueio, incremento: -incremento };
    case 2:
      return bloqueioEfetivado;
    case 3:
      return bloqueioAntena;
    case 4:
      return bloqueioPraFora;
    case 5:
      return diretoPraFora;
    case 6:
      return { ...porCimaDoBloqueio, incremento };
    default:
      break;
  }
}
