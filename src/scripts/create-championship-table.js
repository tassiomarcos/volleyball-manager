export function createRegularChampionship(data, withReturn = true) {
  // número de rodadas do campeonatos
  let rodadas = data.length - 1;

  if (withReturn) rodadas = rodadas * 2;

  //número de partidas por rodada
  const numeroDePartidas = data.length / 2;

  //array 2

  // array que sera salvo as rodas
  const confrontos = [];

  // array da rodada
  let rodada = [];

  let temp = null;
  let verAux = 0;

  for (let i = 1; i <= rodadas; i++) {
    let time1 = 0;
    let time2 = data.length - 1;
    for (let j = 1; j <= numeroDePartidas; j++) {
      if (i <= rodadas / 2) {
        if (temp === null) {
          temp = data;
        }
        if (i > 1 && verAux !== i) {
          verAux = i;

          temp[temp.length] = temp[1];
          temp.splice(1, 1);

          rodada.push({
            visitante: temp[time1],
            mandante: temp[time2],
            terminada: false,
            sets: [],
            estatisticas: [],
          });
        } else {
          rodada.push({
            visitante: temp[time1],
            mandante: temp[time2],
            terminada: false,
            sets: [],
            estatisticas: [],
          });
        }

        if (i === rodadas / 2) temp = null;
      } else {
        if (temp === null) {
          temp = data.reverse();
        }

        if (i > rodadas / 2 + 1 && verAux !== i) {
          verAux = i;

          temp[temp.length] = temp[1];
          temp.splice(1, 1);

          rodada.push({
            visitante: temp[time1],
            mandante: temp[time2],
            terminada: false,
            sets: [],
            estatisticas: [],
          });
        } else {
          rodada.push({
            visitante: temp[time1],
            mandante: temp[time2],
            terminada: false,
            sets: [],
            estatisticas: [],
          });
        }
      }

      time1 += 1;
      time2 -= 1;
    }

    confrontos.push(rodada);
    rodada = [];
  }

  return confrontos;
}

export function createMataMata(data, goAndBack) {
  let jogos = [];
  let time1 = 0;
  let time2 = data.length - 1;

  let is2Games = goAndBack ? data.length / 2 : data.length;

  for (let i = 0; i < is2Games; i++) {
    jogos.push({
      visitante: data[time1],
      mandante: data[time2],
      terminada: false,
      sets: [],
      estatisticas: [],
    });

    time1 += 1;
    time2 -= 1;
  }

  return jogos;
}

export function createGroupPhases() {}
